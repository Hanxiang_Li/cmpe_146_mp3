#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "lpc40xx.h"
#include "adc.h"

/*enum type*/
enum StickerDir { js_none, js_left, js_right, js_up, js_down };
/*get joy sticker direction*/
enum StickerDir joysticker_get_dir(void);
/*Get Button Status*/
// bool selection(void);
// bool cancle(void);

static const uint16_t dec_vol_0=0;
static const uint16_t dec_vol_1=0x2020;
static const uint16_t dec_vol_2=0x4040;
static const uint16_t dec_vol_3=0x6060;
uint16_t get_dec_vol(void);