#include "adc.h"
#include "gpio.h"
#include "uart.h"
#include <stdio.h>

#include "lpc40xx.h"
#include "joysticker_init.h"

void joysticker_init(void) {
  adc__initialize();
  LPC_IOCON->P0_25 |= (0x1 << 0); //adc 2
  LPC_IOCON->P1_30 |= (0x3 << 0); //adc 4
  LPC_IOCON->P1_31 |= (0x3 << 0); //adc 5

  LPC_IOCON->P0_25 &= ~(0x1 << 7);
  LPC_IOCON->P1_30 &= ~(0x1 << 7);
  LPC_IOCON->P1_31 &= ~(0x1 << 7);

  //button
  // /* set as input P1_20 && P1_23 */
  // LPC_GPIO1->DIR &= ~(1 << 20);
  LPC_GPIO1->DIR &= ~(1 << 23);
  // /* set as PULL DOWN */
  // LPC_IOCON->P1_20 |= (1 << 3);
  // LPC_IOCON->P1_20 &= ~(1 << 4);
  LPC_IOCON->P1_23 |= (1 << 3);
  LPC_IOCON->P1_23 &= ~(1 << 4);
}
