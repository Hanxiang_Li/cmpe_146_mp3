#include "joysticker.h"

static const int right_stand = 3000;
// static const int left_stand = 2200;
static const int up_stand = 3000;
// static const int down_stand = 2200;
static const int normal_stand = 2300;

/*Get Button Status*/
static bool gpio1__get_level(uint8_t pin_num) { return LPC_GPIO1->PIN & (1 << pin_num); }

bool selection(void) {
  bool stat_selection;
  stat_selection = gpio1__get_level(20);
  if (stat_selection == 0)
    return false;
  else
    return true;
}

bool cancle(void) {
  bool stat_cancle;
  stat_cancle = gpio1__get_level(23);
  if (stat_cancle == 0)
    return false;
  else
    return true;
}
enum StickerDir joysticker_get_dir(void) {
  int x_val = 0;
  int y_val = 0;
  int check_val_x = 0;
  int check_val_y = 0;

  enum StickerDir dir_status;

  x_val = adc__get_adc_value(ADC__CHANNEL_5);
  y_val = adc__get_adc_value(ADC__CHANNEL_2);
  check_val_x = abs(x_val - normal_stand);
  check_val_y = abs(y_val - normal_stand);
  /*The sticker doesn't move*/
  if (check_val_x < 700 && check_val_y < 700) {
    dir_status = js_none;
  }
  /*When the Sticker is closer to X-asix*/
  else if (check_val_x > check_val_y) {
    if (x_val > right_stand) {
      dir_status = js_right;
    } else {
      dir_status = js_left;
    }
  }
  /*when the Sticker is closer to Y-asix*/
  else {
    if (y_val > up_stand) {
      dir_status = js_up;
    } else {
      dir_status = js_down;
    }
  }
  // printf("val of x:%u\t val of y :%u\n", x_val, y_val);
  // printf("val of abs_x:%u\t val of abs_y :%u\n", check_val_x, check_val_y);
  /*return as the enum_type*/
  switch (dir_status) {
  case 0:
    return js_none;
    break;
  case 1:
    return js_left;
    break;
  case 2:
    return js_right;
    break;
  case 3:
    return js_up;
    break;
  case 4:
    return js_down;
    break;
  default:
    return js_none;
    break;
  }
}

uint16_t get_dec_vol(void) {
  int adc_vol = adc__get_adc_value(ADC__CHANNEL_4);
  if(adc_vol<1000)
    return dec_vol_0;
  else if(adc_vol<2000)
    return dec_vol_1;
  else if(adc_vol<3000)
    return dec_vol_2;
  else
    return dec_vol_3;
}