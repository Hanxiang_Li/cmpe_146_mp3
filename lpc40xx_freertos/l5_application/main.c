#include <stdio.h>
#include <string.h>

#include "FreeRTOS.h"
#include "task.h"

#include "board_io.h"
#include "common_macros.h"
#include "gpio.h"
#include "sj2_cli.h"

#include "ff.h"
#include "cli_handlers.h"

#include "lcd.h"
#include "gpio_isr.h"
#include "ssp2_lab.h"

#include "delay.h"
#include "lpc_peripherals.h"
#include "semphr.h"
#include "uart.h"
#include "uart_printf.h"

#include "sound_test.h"
#include "joysticker_init.h"
#include "joysticker.h"

enum {mode_none, mode_menu, mode_songlist, mode_setting, mode_play, mode_pause} mp3_mode;
enum {menu_select_none, menu_song_list, menu_setting} mp3_menu_select;
typedef char songname[32];
const int length_512 = 512;
QueueHandle_t Q_songname;
QueueHandle_t Q_songdata;
QueueHandle_t Q_filename;
QueueHandle_t Q_menu_scroll;
char song_list[32][32];
int song_number=0;
int song_index=0;
SemaphoreHandle_t xSemaphore_menu;
SemaphoreHandle_t xSemaphore_song_list;
uint16_t mp3_dec_vol=0;
uint16_t mp3_dec_bass=0;
SemaphoreHandle_t xSemaphore_mutex_spi;

static const uint16_t dec_bass_0=0;
static const uint16_t dec_bass_1=0x00F6;

static gpio_s switch0, sw1, sw2, sw3;

void mp3_polling_task(void *p){
  enum StickerDir js_dir;
  uint16_t vol;
  while(1){
    while(1){
      js_dir=joysticker_get_dir();
      printf("%d\n", js_dir);
      vol=get_dec_vol();
      if(((LPC_GPIO0->PIN>>29) & 1)){
        xSemaphoreGive(xSemaphore_menu);
        break;
      }else if(mp3_mode==mode_menu){
        if(js_dir==js_up){
          // printf("up\n");
          xQueueSend(Q_menu_scroll, &js_dir, portMAX_DELAY);
          break;
        }else if(js_dir==js_down){
          // printf("down\n");
          xQueueSend(Q_menu_scroll, &js_dir, portMAX_DELAY);
          break;
        }else if(((LPC_GPIO0->PIN>>30) & 1)){
          xSemaphoreGive(xSemaphore_song_list);
          break;
        }else if(mp3_dec_vol!=vol){
          mp3_dec_vol=vol;
          mp3_writeRequest(0x0B, mp3_dec_vol);
          break;
        }
      }else if(mp3_mode==mode_songlist){
        if(js_dir==js_up){
          // printf("up\n");
          if(song_index==0)
            song_index=2;
          else
            song_index-=1;
          break;
        }else if(js_dir==js_down){
          // printf("down\n");
          if(song_index==2)
            song_index=0;
          else
            song_index+=1;
          break;
        }else if(mp3_mode==mode_songlist && ((LPC_GPIO0->PIN>>30) & 1)){
          printf("select song\n");
          xQueueSend(Q_songname, &song_list[song_index], portMAX_DELAY);
          mp3_mode=mode_play;
          break;
        }
      }else if(mp3_mode==mode_play || mp3_mode==mode_pause){
        // printf("0_29: %d\n", ((LPC_GPIO0->PIN >> 29) & 1));
        // printf("0_30: %d\n", ((LPC_GPIO0->PIN >> 30) & 1));
        // printf("1_15: %d\n", ((LPC_GPIO1->PIN >> 15) & 1));
        // printf("1_19: %d\n", ((LPC_GPIO1->PIN >> 19) & 1));
        if((LPC_GPIO0->PIN >> 30) & 1){
          if(mp3_mode==mode_play)
            mp3_mode=mode_pause;
          else if(mp3_mode==mode_pause)
            mp3_mode=mode_play;
          break;
        }
        else if(mp3_dec_vol!=vol){
          mp3_dec_vol=vol;
          mp3_writeRequest(0x0B, mp3_dec_vol);
          break;
        }else if(js_dir==js_left && mp3_mode==mode_play){
          mp3_mode=mode_menu;
          vTaskDelay(100);
          if(song_index==0)
            song_index=2;
          else
            song_index-=1;
          xQueueSend(Q_songname, &song_list[song_index], portMAX_DELAY);
          mp3_mode=mode_play;
        }else if(js_dir==js_right && mp3_mode==mode_play){
          mp3_mode=mode_menu;
          vTaskDelay(100);
          if(song_index==2)
            song_index=0;
          else
            song_index+=1;
          xQueueSend(Q_songname, &song_list[song_index], portMAX_DELAY);
          mp3_mode=mode_play;
        }else if(((LPC_GPIO1->PIN >> 23) & 1) && mp3_mode==mode_play){
          if(mp3_dec_bass==dec_bass_0)
            mp3_dec_bass=dec_bass_1;
          else
            mp3_dec_bass=dec_bass_0;
          mp3_writeRequest(0x0B, mp3_dec_bass);
          break;
        }
      }
      vTaskDelay(100);
    }
    vTaskDelay(600);
  }
}
void mp3_menu_scroll_task(void *p){
  enum StickerDir js_dir;
  while(1){
    xQueueReceive(Q_menu_scroll, &js_dir, portMAX_DELAY);
    if(mp3_menu_select==menu_song_list){
      lcd_print_char_8x8(2, 0, 0, 's');
      lcd_print_char_8x8(2, 0, 8, 'o');
      lcd_print_char_8x8(2, 1, 0, 'n');
      lcd_print_char_8x8(2, 1, 8, 'g');
      lcd_print_char_8x8(2, 2, 0, '_');
      lcd_print_char_8x8(2, 2, 8, 'l');
      lcd_print_char_8x8(2, 3, 0, 'i');
      lcd_print_char_8x8(2, 3, 8, 's');
      lcd_print_char_8x8(2, 4, 0, 't');
    }else if(mp3_menu_select==menu_setting){
      lcd_print_char_8x8(3, 0, 0, 's');
      lcd_print_char_8x8(3, 0, 8, 'e');
      lcd_print_char_8x8(3, 1, 0, 't');
      lcd_print_char_8x8(3, 1, 8, 't');
      lcd_print_char_8x8(3, 2, 0, 'i');
      lcd_print_char_8x8(3, 2, 8, 'n');
      lcd_print_char_8x8(3, 3, 0, 'g');
    }
    if(js_dir==js_up){
      // printf("scroll_up\n");
      if(mp3_menu_select==menu_select_none)
        mp3_menu_select=menu_setting;
      else
        mp3_menu_select-=1;
    }else if(js_dir==js_down){
      // printf("scroll_down\n");
      if(mp3_menu_select==menu_setting)
        mp3_menu_select=menu_select_none;
      else
        mp3_menu_select+=1;
    }
    vTaskDelay(10);
    if(mp3_menu_select==menu_song_list){
      lcd_print_char_select_8x8(2, 0, 0, 's');
      lcd_print_char_select_8x8(2, 0, 8, 'o');
      lcd_print_char_select_8x8(2, 1, 0, 'n');
      lcd_print_char_select_8x8(2, 1, 8, 'g');
      lcd_print_char_select_8x8(2, 2, 0, '_');
      lcd_print_char_select_8x8(2, 2, 8, 'l');
      lcd_print_char_select_8x8(2, 3, 0, 'i');
      lcd_print_char_select_8x8(2, 3, 8, 's');
      lcd_print_char_select_8x8(2, 4, 0, 't');
    }else if(mp3_menu_select==menu_setting){
      lcd_print_char_select_8x8(3, 0, 0, 's');
      lcd_print_char_select_8x8(3, 0, 8, 'e');
      lcd_print_char_select_8x8(3, 1, 0, 't');
      lcd_print_char_select_8x8(3, 1, 8, 't');
      lcd_print_char_select_8x8(3, 2, 0, 'i');
      lcd_print_char_select_8x8(3, 2, 8, 'n');
      lcd_print_char_select_8x8(3, 3, 0, 'g');
    }
    vTaskDelay(10);
  }
}
// void mp3_lcd_print_line(uint8_t page, uint8_t * data){
//   int data_length=0;
//   while(data[data_length]!='\0')
//     ++data_length;
//   int k=0;
//   int loop_i;
//   if((data_length % 2) == 0)
//     loop_i =data_length/2;
//   else{
//     loop_i =data_length/2 +1;
//   }
//   for (int i = 0; i < loop_i; ++i)
//   {
//     for (int j = 0; j < 2; ++j)
//     {
//       lcd_print_char_select_8x8(page, i, k, data[i*2+j]);
//       if(k==8)
//         k=0;
//       else
//         k=8;
//     }
//   }
// }

void mp3_menu_task(void *p){
  int page = 2;
  songname name;
  char menu[5]="menu";
  char songlist[9]="songlist";
  while(1){
    vTaskDelay(1000);
    if( xSemaphoreTake( xSemaphore_menu,100000) == pdTRUE ){
      mp3_mode=mode_menu;
      lcd_clear();

      // int k=0;
      // for (int i = 0; i < 2; ++i)
      // {
      //   for (int j = 0; j < 2; ++j)
      //   {
      //     lcd_print_char_8x8(0, i, k, menu[i*2+j]);
      //     if(k==8)
      //       k=0;
      //     else
      //       k=8;
      //   }
      // }

      // k=0;
      // for (int i = 0; i < 4; ++i)
      // {
      //   for (int j = 0; j < 2; ++j)
      //   {
      //     lcd_print_char_8x8(2, i, k, songlist[i*2+j]);
      //     if(k==8)
      //       k=0;
      //     else
      //       k=8;
      //   }
      // }


      lcd_print_char_8x8(0, 0, 0, 'm');
      lcd_print_char_8x8(0, 0, 8, 'e');
      lcd_print_char_8x8(0, 1, 0, 'n');
      lcd_print_char_8x8(0, 1, 8, 'u');

      lcd_print_char_8x8(2, 0, 0, 's');
      lcd_print_char_8x8(2, 0, 8, 'o');
      lcd_print_char_8x8(2, 1, 0, 'n');
      lcd_print_char_8x8(2, 1, 8, 'g');
      lcd_print_char_8x8(2, 2, 0, '_');
      lcd_print_char_8x8(2, 2, 8, 'l');
      lcd_print_char_8x8(2, 3, 0, 'i');
      lcd_print_char_8x8(2, 3, 8, 's');
      lcd_print_char_8x8(2, 4, 0, 't');

      lcd_print_char_8x8(3, 0, 0, 's');
      lcd_print_char_8x8(3, 0, 8, 'e');
      lcd_print_char_8x8(3, 1, 0, 't');
      lcd_print_char_8x8(3, 1, 8, 't');
      lcd_print_char_8x8(3, 2, 0, 'i');
      lcd_print_char_8x8(3, 2, 8, 'n');
      lcd_print_char_8x8(3, 3, 0, 'g');

      // lcd_print_char_select_8x8(4, 0, 0, 'a');

      // lcd_print_line_8x8(7, "menu");
      // lcd_print_line_8x8(2, "song_list");
      // lcd_print_line_8x8(3, "setting");
    }
  }
}

void mp3_song_list_task(void *p){
  int page = 2;
  songname name;
  char menu[5]="menu";
  char songlist[9]="songlist";
  while(1){
    vTaskDelay(1000);
    if( xSemaphoreTake( xSemaphore_song_list, 100000) == pdTRUE ){
      mp3_mode=mode_songlist;
      lcd_clear();
      xQueueSend(Q_filename, &song_list, portMAX_DELAY);
    }
  }
}

app_cli_status_e cli__mp3_play(app_cli__argument_t argument, sl_string_t user_input_minus_command_name,
                                   app_cli__print_string_function cli_output) {  
  sl_string_t s = user_input_minus_command_name;
  songname name;
  // for(int i=0; i<32;i++){
  //   if(s[i] == '\0')
  //     break;
  //   *name = sl_string__c_str(&s);
  // }

  // sl_string__printf(s, "\n");
  int c=0;
  while(c<32-5){
    if(s[c]=='.'){
      break;
    }
    // sl_string__append_char(s, s[c]);
    name[c] = sl_string__c_str(s[c]);
    c++;
  }
  for(int i=0;i<4;i++){
    name[c] = sl_string__c_str(s[c]);
    c++;
  }
  name[c] = '\0';
  // cli_output(NULL, s);

  // printf("\nsize: %d\nname: %s\ns: %s\nname 1 : %c\nname 2 : %c\n", sl_string__get_length(s), name, s, name[0], name[1]);
  xQueueSend(Q_songname, &name, portMAX_DELAY);

  // printf("Sent %s over to the Q_songname\n", user_input_minus_command_name);
  return APP_CLI_STATUS__SUCCESS;
}

bool mp3_dreq_getLevel(void){
  return ((LPC_GPIO0->PIN >> 8) & 1);
}
bool mp3_decoder_needs_data(void){
  return mp3_dreq_getLevel();
}

// Reader tasks receives song-name over Q_songname to start reading it
void mp3_reader_task(void *p) {
  songname name;
  char bytes_512[length_512];
  FIL file;
  UINT byte_read = 0;
  int file_size;
  int byte_been_read;
  while(1) {
    xQueueReceive(Q_songname, &name, portMAX_DELAY);
    printf("Received song to play: %s\n", name);
    const char *filename = name;
    FRESULT result = f_open(&file, filename, FA_READ);

    if (FR_OK == result) {
      file_size = f_size(&file);
      printf("file_size: %u\n", file_size);
      byte_been_read = 0;
      while(byte_been_read<file_size){
        f_read(&file, &bytes_512, length_512, &byte_read);
        xQueueSend(Q_songdata, &bytes_512, portMAX_DELAY);
        byte_been_read += byte_read;
        while(mp3_mode == mode_pause)
            vTaskDelay(1000);
        if(mp3_mode==mode_menu)
          break;
      }
      f_close(&file);
      printf("\nend of file. \n");
      printf("from reader: size %d real_size %d\n", file_size, byte_been_read);
    }else{
      printf("ERROR: Failed to open: %s\n", filename);
    }
  }
}
// Player task receives song data over Q_songdata to send it to the MP3 decoder
void mp3_player_task(void *p) {
  char bytes_512[length_512];
  while (1) {
    xQueueReceive(Q_songdata, &bytes_512, portMAX_DELAY);
    dec_dcs();
    vTaskDelay(6);
    for (int i = 0; i < sizeof(bytes_512); i+=32) {
      while (!mp3_decoder_needs_data()) {
        vTaskDelay(5);
      }
      for(int j=0;j<32;j++){
        ssp0__exchange_byte_lab(bytes_512[i+j]);
      }
      while (!mp3_decoder_needs_data()) {
        vTaskDelay(5);
      }
    }
    dec_dds();
  }
}

FRESULT scan_files (char* path){
  FRESULT res;
  DIR dir;
  UINT i;
  static FILINFO fno;

  res = f_opendir(&dir, path);                       /* Open the directory */
  if (res == FR_OK) {
      for (;;) {
          res = f_readdir(&dir, &fno);                   /* Read a directory item */
          if (res != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
          if (fno.fattrib & AM_DIR) {                    /* It is a directory */
              i = strlen(path);
              sprintf(&path[i], "/%s", fno.fname);
              res = scan_files(path);                     /* Enter the directory */
              if (res != FR_OK) break;
              path[i] = 0;
          } else {                                       /* It is a file. */
              // printf("%s/%s\n", path, fno.fname);

              // find mp3
              if(fno.fname[strlen(fno.fname)-1]=='3'){
                int i=0;
                while(fno.fname[i]!='\0'){
                  song_list[song_number][i]=fno.fname[i];
                  ++i;
                }
                // printf("%s\n", song_list[song_number]);
                song_number++;
                // int i=0;
                // while(fno.fname[i]!='\0'){
                //   printf("%c", fno.fname[i]);
                //   i++;
                // }
                // printf("\n");
                // xQueueSend(Q_filename, &fno.fname, portMAX_DELAY);
              }
          }
      }
      f_closedir(&dir);
  }

  return res;
}

int length_of_name(songname *name){
  int i=0;
  while(i<32){
    if(name[i]=='\0')
      return i;
  }
  return i;
}

void mp3_list_task(void *p){
  int page = 2;
  songname name;
  while(1){
    xQueueReceive(Q_filename, &name, portMAX_DELAY);
    lcd_print_line_8x8(page, name);
    if(page>6){
      page=2;
    }else{
      page++;
    }
    vTaskDelay(10);
    // while (!((LPC_GPIO0->PIN>>29) & 1)) {
    //   vTaskDelay(100);
    // }
    // vTaskDelay(1000);
  }
}

int mp3_song_list_read(void)
{
  FATFS fs;
  FRESULT res;
  char buff[256];

  res = f_mount(&fs, "", 1);
  if (res == FR_OK) {
    strcpy(buff, "/");
    res = scan_files(buff);
  }

  return res;
}
void mp3_song_list_read_tast(void *p){
  while(1){
    vTaskDelay(10);
    printf("test start\n");
    mp3_song_list_read();
    vTaskDelay(3600*1000);
  }
}


uint16_t MODE = 0x4810; // 4800
uint16_t CLOCKF = 0xE000; //was 9800, EBE8, B3E8, BBE8
volatile uint16_t VOL = 0x2222; //full vol
uint16_t BASS = 0x0076; //was 00F6
uint16_t AUDATA = 0xAC80; //for stereo decoding, AC45,AC80, BB80-check
uint16_t STATUS;
//VS10xx SCI Registers
#define SCI_MODE 0x00
#define SCI_STATUS 0x01
#define SCI_BASS 0x02
#define SCI_CLOCKF 0x03
#define SCI_DECODE_TIME 0x04
#define SCI_AUDATA 0x05
#define SCI_WRAM 0x06
#define SCI_WRAMADDR 0x07
#define SCI_HDAT0 0x08
#define SCI_HDAT1 0x09
#define SCI_AIADDR 0x0A
#define SCI_VOL 0x0B
#define SCI_AICTRL0 0x0C
#define SCI_AICTRL1 0x0D
#define SCI_AICTRL2 0x0E
#define SCI_AICTRL3 0x0F
#define playSpeed 0x1e04

//commands
#define READ 0x03
#define WRITE 0x02

//static uint8_t SEND_NUM_BYTES = 32;
static uint16_t READ_BYTES_FROM_FILE = 4096;
static uint16_t TRANSMIT_BYTES_TO_DECODER = 32;

void mp3_writeRequest(uint8_t address, uint16_t data)
{
  while (!mp3_dreq_getLevel())
    ;
  dec_cs(); //do a chip select
  ssp0__exchange_byte_lab(WRITE);
  ssp0__exchange_byte_lab(address);
  uint8_t dataUpper = (data >> 8 & 0xFF);
  uint8_t dataLower = (data & 0xFF);
  ssp0__exchange_byte_lab(dataUpper);
  ssp0__exchange_byte_lab(dataLower);
  while (!mp3_dreq_getLevel())
    ; // dreq should go high indicating transfer complete
  dec_ds(); // Assert a HIGH signal to de-assert the CS
}
void mp3_initDecoder()
{
  mp3_writeRequest(SCI_MODE, MODE);
  mp3_writeRequest(SCI_CLOCKF, CLOCKF);
  // mp3_writeRequest(SCI_VOL, VOL);
  // mp3_writeRequest(SCI_BASS, BASS);
  // mp3_writeRequest(SCI_AUDATA, AUDATA);

  // mp3_writeRequest(SCI_AICTRL0, 0x7000);
  // mp3_writeRequest(SCI_AICTRL1, 0x7000);
  // mp3_writeRequest(SCI_AIADDR, 0x4020);
}

void dec_cs(void){
  if( xSemaphoreTake( xSemaphore_mutex_spi, 100000) == pdTRUE ){
    LPC_IOCON->P0_6 &= ~(7<<0);
    LPC_GPIO0->DIR |= (1<<6);
    LPC_GPIO0->CLR |= (1<<6);
  }
}
void dec_ds(void){
  LPC_IOCON->P0_6 &= ~(7<<0);
  LPC_GPIO0->DIR |= (1<<6);
  LPC_GPIO0->SET |= (1<<6);
  xSemaphoreGive( xSemaphore_mutex_spi );
}
void dec_dcs(void){
  if( xSemaphoreTake( xSemaphore_mutex_spi, 100000) == pdTRUE ){
    LPC_IOCON->P0_7 &= ~(7<<0);
    LPC_GPIO0->DIR |= (1<<7);
    LPC_GPIO0->CLR |= (1<<7);
  }
}
void dec_dds(void){
  LPC_IOCON->P0_7 &= ~(7<<0);
  LPC_GPIO0->DIR |= (1<<7);
  LPC_GPIO0->SET |= (1<<7);
  xSemaphoreGive( xSemaphore_mutex_spi );
}
void dec_rst(void){
  LPC_IOCON->P0_9 &= ~(7<<0);
  LPC_GPIO0->DIR |= (1<<9);
  LPC_GPIO0->CLR |= (1<<9);
  vTaskDelay(100);
  LPC_GPIO0->SET |= (1<<9);
  vTaskDelay(100);
}
void dec_dreq_set_as_input(void){
  LPC_IOCON->P0_8 &= ~(0x7<<0);
  LPC_GPIO0->DIR &= ~(1<<8);
}
void spi_ds_1_10(void){
  LPC_IOCON->P1_10 &= ~(7<<0);
  LPC_GPIO1->DIR |= (1<<10);
  LPC_GPIO1->SET |= (1<<10);
}
void dec_init(){
  spi_ds_1_10();

  dec_dreq_set_as_input();
  dec_ds();
  dec_dds();

  vTaskDelay(100);
  dec_rst();

  vTaskDelay(100);
  mp3_initDecoder();
  vTaskDelay(100);
}
typedef struct {
  uint8_t first_byte;
  uint8_t second_byte;
  uint8_t third_byte;
  uint8_t forth_byte;
} mp3_dec;
mp3_dec mp3_dec_read(uint8_t first, uint8_t second, uint8_t third, uint8_t forth) {
  mp3_dec data = { 1, 2, 3, 4 };
  
  dec_cs();
  {
    // Send opcode and read bytes
    // TODO: Populate members of the 'adesto_flash_id_s' struct
    // ssp0__exchange_byte_lab(0x03);
    data.first_byte = ssp0__exchange_byte_lab(first);
    data.second_byte = ssp0__exchange_byte_lab(second);
    data.third_byte = ssp0__exchange_byte_lab(third);
    data.forth_byte = ssp0__exchange_byte_lab(forth);
  }
  dec_ds();

  return data;
}

mp3_dec mp3_dec_d_write(uint8_t first, uint8_t second, uint8_t third, uint8_t forth) {
  dec_dcs();
  {
    ssp0__exchange_byte_lab(first);
    ssp0__exchange_byte_lab(second);
    ssp0__exchange_byte_lab(third);
    ssp0__exchange_byte_lab(forth);
    ssp0__exchange_byte_lab(0);
    ssp0__exchange_byte_lab(0);
    ssp0__exchange_byte_lab(0);
    ssp0__exchange_byte_lab(0);
  }
  dec_dds();
}

void task_spi_task(void *pvParameters){
  mp3_dec id={0x01, 0x02, 0x03, 0x04};
  vTaskDelay(1000);
  dec_init();
  vTaskDelay(100);
  // mp3_dec_d_write(0x53, 0xEF, 0x6E, 0x7E);
  // mp3_dec_d_write(0, 0, 0, 0);
  for(uint8_t i=0; i<16;i++){
    id=mp3_dec_read(0x03, i, 0x12, 0x34);
    printf("dec_write: %x, %x, %x, %x\n", id.first_byte, id.second_byte, id.third_byte, id.forth_byte);
    vTaskDelay(100);
  }
  int i=0;
  while(1){
    vTaskDelay(500000);
    // printf("%d\n", i);
    // ++i;
  }
}
void mp3_lcd_init(void){
  lcd_init();
  lcd_clear();
  lcd_font_init();
  lcd_print_line_8x8(0, "--mp3--");
}
void mp3_list_init(void){
  for(int i=0;i<32;i++){
    for(int j=0;j<32;j++){
      song_list[i][j]='\0';
    }
  }
}
void mp3_js_init(void){
  joysticker_init();
}

void mp3_init(void){
  mp3_lcd_init();
  mp3_list_init();
  // mp3_dec();
  mp3_js_init();
}

int main(void) {
  sj2_cli__init();

  Q_songname = xQueueCreate(1, 32);
  Q_songdata = xQueueCreate(1, length_512);
  Q_filename = xQueueCreate(1, 32);

  xSemaphore_menu = xSemaphoreCreateBinary();
  xSemaphore_song_list = xSemaphoreCreateBinary();
  Q_menu_scroll = xQueueCreate(1, sizeof(enum StickerDir));

  xSemaphore_mutex_spi = xSemaphoreCreateMutex();

  switch0 = gpio__construct_as_input(0, 29);
  sw1 = gpio__construct_as_input(0, 30);
  // sw2 = gpio__construct_as_input(1, 15);
  // sw3 = gpio__construct_as_input(1, 19);

  mp3_init();

  ssp0__init_lab(1);
  // lcd_print_line_8x8(5, "yesrtdyfug");

  delay__ms(100);

  xTaskCreate(mp3_list_task, "list", (512U * 8 / sizeof(void *)), NULL, 1, NULL);
  xTaskCreate(mp3_song_list_read_tast, "mp3_song_list_read", (512U * 8 / sizeof(void *)), NULL, 2, NULL);

  xTaskCreate(mp3_polling_task, "mp3_polling", (512U * 8 / sizeof(void *)), NULL, 5, NULL);
  xTaskCreate(mp3_menu_task, "mp3_menu", (512U * 8 / sizeof(void *)), NULL, 4, NULL);
  xTaskCreate(mp3_song_list_task, "mp3_song_list", (512U * 8 / sizeof(void *)), NULL, 4, NULL);

  xTaskCreate(mp3_menu_scroll_task, "mp3_menu_scroll", (512U * 8 / sizeof(void *)), NULL, 5, NULL);

  xTaskCreate(task_spi_task, "dec_spi", (512U * 8 / sizeof(void *)), NULL, 5, NULL);
  xTaskCreate(mp3_reader_task, "reader", (512U * 8 / sizeof(void *)), NULL, 4, NULL);
  xTaskCreate(mp3_player_task, "player", (512U * 8 / sizeof(void *)), NULL, 5, NULL);


  vTaskStartScheduler();
  return 0;
}
